let processor = {
    timerCallback: function() {
      if (this.video.paused || this.video.ended) {
        return;
      }
      this.computeFrame();
      let self = this;
      setTimeout(function () {
          self.timerCallback();
        }, 0);
    },
  
    doLoad: function() {

      let videoResizeRate = 8;

      this.video = document.getElementById("video");
      this.c1 = document.getElementById("c1");
      this.ctx1 = this.c1.getContext("2d");
      this.c2 = document.getElementById("c2");
      this.ctx2 = this.c2.getContext("2d");

      
      let self = this;
      this.video.addEventListener("play", function() {
          self.width = self.video.videoWidth / videoResizeRate;
          self.height = self.video.videoHeight / videoResizeRate;
          self.timerCallback();
        }, false);
    },
  
    computeFrame: function() {
      this.ctx1.drawImage(this.video, 0, 0, this.width, this.height);
      
      let frame = this.ctx1.getImageData(0, 0, this.width, this.height);
          let l = frame.data.length / 4;
  
      let x = 0;
      let y = 0;

      let box_x1 = 100;
      let box_y1 = 40;        
      let box_x2 = 220;
      let box_y2 = 150;

      if(this.video.currentTime > 20) {
        box_x1 = 120;
        box_y1 = 60;        
        box_x2 = 240;
        box_y2 = 170;
      }
      else
      if(this.video.currentTime > 30)      
      {
        box_x1 = 140;
        box_y1 = 80;        
        box_x2 = 260;
        box_y2 = 190;
      }

      for (let i = 0; i < l; i++, x++) {
        if(x >= this.width) {
          x=0;
          y++;
        }

        let r = frame.data[i * 4 + 0];
        let g = frame.data[i * 4 + 1];
        let b = frame.data[i * 4 + 2];
        //if (g > 100 && r > 100 && b < 43)
        
        if(this.video.currentTime > 1 && this.video.currentTime < 40 && 
            (
              //((x >= box_x1 && (x < box_x1 + 2 || (y == box_y1) )) || ( x >= box_x2-2 && x < box_x2)) && (y >= box_y1 && y <= box_y2 ) ||  
              ((y == box_y1 || y == box_y2) && x >= box_x1 && x < box_x2) ||
              ((x == box_x1 || x == box_x2) && y >= box_y1 && y < box_y2)
            )
          ) {
          frame.data[i * 4 + 0]=0;
          frame.data[i * 4 + 1]=222;
          frame.data[i * 4 + 2]=255;
        }
        else
        if (g > 200 && r > 0 && b < 255) {          
          frame.data[i * 4 + 3] = 0;  // Canal alfa
        }

        /*
        if (g > 200 && r > 0 && b < 255) {
          frame.data[i * 4 + 0]=230;
          frame.data[i * 4 + 1]=49;
          frame.data[i * 4 + 2]=182;        
        } 
        */                
         
      }
      this.ctx2.putImageData(frame, 0, 0);
      return;
    }
  };

document.addEventListener("DOMContentLoaded", () => {
  processor.doLoad();
});
